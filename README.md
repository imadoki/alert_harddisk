# Alert HardDisk

指定のパスの残りの容量を調べ、指定の容量以下であればslackに通知を送る  
オプションは全て環境変数として渡す

## オプション

* TARGET_PATH
  * 容量を調べるパス
* THRESHOLD_PERCENTAGE
  * 通知を送るハードディスクの残量
* SLACK_WEBHOOK_URL
  * slackのincoming webhooksのurl