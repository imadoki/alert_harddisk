require 'optparse'
require 'sys/filesystem'
require 'slack/incoming/webhooks'

## オプション
# target_path: 調べるパス
# threshold_percentage: slackへアラートを流す境界値(%)
# slack_webhook_url: slackへ通知するためのwebhook url
params = ARGV.getopts('', 'target_path:', 'threshold_percentage:', 'slack_webhook_url:')
if params['target_path'].nil? || params['threshold_percentage'].nil? || params['slack_webhook_url'].nil?
  puts "target_path, threshold_percentage, slack_webhook_urlは必須です"
  p params
  return
end

# get remaining quantity of target disk
target_path = params['target_path']
threshold_percentage = params['threshold_percentage'].to_f

stat = Sys::Filesystem.stat(target_path)

total = (stat.blocks * stat.block_size).to_f / 1024 / 1024 / 1024
available = (stat.blocks_available * stat.block_size).to_f / 1024 / 1024 / 1024

available_per_total = available / total * 100.0

puts "available: #{available}GB"
puts "total: #{total}GB"
puts "available_per_total: #{available_per_total}%"

# post webhooks
if threshold_percentage > available_per_total
    slack = Slack::Incoming::Webhooks.new params['slack_webhook_url']
    slack.post "('#{target_path}') 残り: #{available}GB"
end
